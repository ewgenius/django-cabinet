# -*- coding: utf-8 -*-
from django.contrib.admin.sites import AlreadyRegistered
from django.template.response import TemplateResponse
from django.contrib.auth import logout as auth_logout, REDIRECT_FIELD_NAME
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.decorators.cache import never_cache
from django.conf.urls import patterns, url, include
from django.db.models import Model
from django.views.decorators.csrf import csrf_protect
from django.utils import six
from functools import update_wrapper
from django.contrib.auth.forms import AuthenticationForm
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import available_attrs
from functools import wraps

LOGIN_FORM_KEY = 'this_is_the_login_form'


def hidden(view_func):
    def wrapped_view(*args, **kwargs):
        return view_func(*args, **kwargs)
    wrapped_view.hidden = True
    return wraps(view_func, assigned=available_attrs(view_func))(wrapped_view)


class CabinetAuthenticationForm(AuthenticationForm):
    this_is_the_login_form = forms.BooleanField(widget=forms.HiddenInput,
                                                initial=1,
                                                error_messages={'required': _(
                                                                "Please log in again, because your session has expired.")})

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        message = 'ERROR_MESSAGE'

        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                if u'@' in username:
                    try:
                        user = User.objects.get(email=username)
                    except (User.DoesNotExist, User.MultipleObjectsReturned):
                        pass
                    else:
                        if user.check_password(password):
                            message = _("Your e-mail address is not your "
                                        "username."
                                        " Try '%s' instead.") % user.username
                raise forms.ValidationError(message)
            elif not self.user_cache.is_active:
                raise forms.ValidationError(message)
        self.check_for_test_cookie()
        return self.cleaned_data


class CabinetModule(object):
    title = ''
    submodules = {}

    def __init__(self, cabinet, model=None, model_admin=None, name='module'):
        self.model = model
        self.model_admin = model_admin
        self.name = name
        self.cabinet = cabinet

        self._views = self._get_views()
        self._menu = self._get_menu()

    def _get_views(self):
        import inspect
        methods = inspect.getmembers(
            self.__class__, predicate=inspect.ismethod)
        return [{'url': v[0][6:], 'view': getattr(self, v[0])}
                for v in filter(lambda x: x[0][0: 5] == '_view', methods)]

    @property
    def model_form(self):
        if hasattr(self, '_model_form'):
            return self._model_form
        else:
            from django.forms import ModelForm

            class MForm(forms.ModelForm):

                class Meta:
                    model = self.model
            return MForm

    @model_form.setter
    def model_form(self, value):
        self._model_form = value

    @property
    def views(self):
        return self._views

    @property
    def menu(self):
        return self._menu

    def get_urls(self):
        def wrap(view, cacheable=False):
            def wrapper(*args, **kwargs):
                return self.cabinet.cabinet_view(view, cacheable)(*args, **kwargs)
            return update_wrapper(wrapper, view)

        module_urls = patterns('',
                               url(r'^$', wrap(self.index)),
                               url(r'^edit/(?P<object_id>.+)/$', wrap(
                                   self.edit)),
                               url(r'^view/(?P<object_id>.+)/$', wrap(self.view)))

        for view in self.views:
            module_urls += patterns('',
                                    url(r'^%s/$' % view['url'], wrap(view['view'])),)

        return module_urls

    def _get_menu(self):
        result = []

        for view in self.views:
            if not hasattr(view['view'], 'hidden'):
                result.append({
                    'title':  view['view'].short_description if hasattr(view['view'], 'short_description') else view['url'],
                    'url': u'/%s/%s/%s/' % (self.cabinet.app_name, self.name, view['url'])
                })

        return result

    @property
    def index_template(self):
        if hasattr(self, 'template_index'):
            return self.template_index
        else:
            return 'cabinet/index_module.jinja'

    @property
    def edit_template(self):
        if hasattr(self, 'template_edit'):
            return self.template_edit
        else:
            return 'cabinet/index_module.jinja'

    @property
    def view_template(self):
        if hasattr(self, 'template_view'):
            return self.template_view
        else:
            return 'cabinet/index_module.jinja'

    @property
    def urls(self):
        return self.get_urls()

    @property
    def list_display(self):
        if hasattr(self, 'list_th'):
            return self.list_th
        else:
            if self.model:
                return self.model._meta.get_all_field_names()
            else:
                return []

    def index(self, request):
        return TemplateResponse(
            request, self.index_template, {
                'title': self.title
            })

    @csrf_exempt
    def edit(self, request, object_id):
        if self.model:
            obj = self.model.objects.get(id=object_id)

            if request.method == 'POST':
                form = self.model_form(request.POST, instance=obj)
                if form.is_valid():
                    form.save()
                    obj.save()
            else:
                form = self.model_form(instance=obj)

            return TemplateResponse(
                request, self.edit_template, {
                    'title': self.title,
                    'form': form
                })
        else:
            return TemplateResponse(
                request, self.index_template, {
                    'title': self.title
                })

    def view(self, request, object_id):

        if self.model:
            from django.forms import ModelForm

            class MForm(forms.ModelForm):

                class Meta:
                    model = self.model

            obj = self.model.objects.get(id=object_id)

            if request.method == 'POST':
                form = MForm(request.POST, instance=obj)
                if form.is_valid():
                    # form.save()
                    obj.save()
            else:
                form = MForm(instance=obj)

            return TemplateResponse(
                request, self.view_template, {
                    'title': self.title,
                    'form': form
                })
        else:
            return TemplateResponse(
                request, self.view_template, {
                    'title': self.title
                })


class Cabinet(object):

    def __init__(self, name='cabinet', app_name='cabinet'):
        self.name = name
        self.app_name = app_name
        self._modules = {}
        self._views = {}
        self._main_module = None

    @property
    def title(self):
        if hasattr(self, 'title'):
            return self.title
        else:
            return ''

    def register_view(self, viewurl, view, short_description='method_name', in_menu=False):
        def wrap(view, cacheable=False):
            def wrapper(*args, **kwargs):
                return self.cabinet_view(view, cacheable)(*args, **kwargs)
            return update_wrapper(wrapper, view)

        if viewurl in self._views:
            raise AlreadyRegistered(
                'The view %s is already registered' % url)
        else:
            self._views[viewurl] = {
                'url': url(viewurl, wrap(view)),
                'desc': short_description,
                'in_menu': in_menu
            }

    def register(self, module_name, module_class=None, model=None, model_admin=None, is_main=False):
        if module_name in self._modules:
            raise AlreadyRegistered(
                'The module %s is already registered' % module_name)
        else:
            if not module_class:
                module_class = CabinetModule
            self._modules[module_name] = module_class(self,
                                                      model, name=module_name, model_admin=model_admin)
            if is_main and not self._main_module:
                self._main_module = self._modules[module_name]

    def has_permission(self, request):
        return request.user.is_active

    def cabinet_view(self, view, cacheable=False):
        def inner(request, *args, **kwargs):
            if LOGIN_FORM_KEY in request.POST and request.user.is_authenticated():
                auth_logout(request)
            if not self.has_permission(request):
                if request.path == reverse('admin:logout',
                                           current_app=self.name):
                    index_path = reverse(
                        'admin:index', current_app=self.name)
                    return HttpResponseRedirect(index_path)
                return self.login(request)
            return view(request, *args, **kwargs)
        if not cacheable:
            inner = never_cache(inner)
        if not getattr(view, 'csrf_exempt', False):
            inner = csrf_protect(inner)
        return update_wrapper(inner, view)

    def get_urls(self):

        def wrap(view, cacheable=False):
            def wrapper(*args, **kwargs):
                return self.cabinet_view(view, cacheable)(*args, **kwargs)
            return update_wrapper(wrapper, view)

        if self._main_module:
            cabinet_urls = self._main_module.urls
        else:
            cabinet_urls = patterns('',
                                    url(r'^$', wrap(self.index)))

        cabinet_urls += patterns('',
                                 url(r'^logout/$', wrap(
                                     self.logout), name='admin:logout'))

        for viewurl, view in six.iteritems(self._views):
            cabinet_urls += patterns('', view['url'])

        for module_name, module in six.iteritems(self._modules):
            if module is not self._main_module:
                cabinet_urls += patterns('',
                                         url(r'^%s/' % module_name, include(module.urls)))

        return cabinet_urls

    @property
    def urls(self):
        return self.get_urls(), self.app_name, self.name

    @never_cache
    def login(self, request, extra_context=None):
        from django.contrib.auth.views import login
        context = {
            'title': _('Log in'),
            'app_path': request.get_full_path(),
            REDIRECT_FIELD_NAME: request.get_full_path(),
        }
        context.update(extra_context or {})

        defaults = {
            'extra_context': context,
            'current_app': self.name,
            'authentication_form': CabinetAuthenticationForm,
            'template_name': 'cabinet/login.jinja',
        }
        return login(request, **defaults)

    @never_cache
    def logout(self, request, extra_context=None):
        from django.contrib.auth.views import logout
        defaults = {
            'current_app': self.name,
            'extra_context': extra_context or {},
        }
        defaults['template_name'] = 'cabinet/logged_out.jinja'
        return logout(request, **defaults)

    @property
    def menu(self):
        result = [{
                  'url': u'/%s/' % self.app_name,
                  'title': u'Главная'
                  }]

        for viewurl, view in six.iteritems(self._views):
            if view['in_menu']:
                result.append({
                    'title': view['desc'],
                    'url': u'/%s/%s' % (self.app_name, viewurl.translate(None, '$^')),
                })

        for module_name, module in six.iteritems(self._modules):
            if module is not self._main_module:
                result.append({
                    'title': module.title,
                    'url': u'/%s/%s/' % (self.app_name, module_name),
                    'submenus': module.menu,
                })

        return result

    def index(self, request):
        module_dict = {}

        for module_name, module in self._modules.items():
            module_dict[module_name] = {
                'name': module_name,
                'title': module.title,
            }

        module_list = list(six.itervalues(module_dict))

        return TemplateResponse(
            request, 'cabinet/index_cabinet.jinja', {
                'modules': module_list,
            },
            current_app=self.name)

cabinet = Cabinet('cabinet')
